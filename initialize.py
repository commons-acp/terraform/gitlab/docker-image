
import requests
import sys
import gitlab
from string import Template

gitlab_token = sys.argv[1]
project_id = sys.argv[2]
project_repo = sys.argv[3]
gl = gitlab.Gitlab("https://gitlab.com",private_token = gitlab_token)

project = gl.projects.get(project_id)
project_name = project.attributes.get("path")
project_path = project.attributes.get("path_with_namespace")



d = {
    'project_id' : project_id,
    'project_name' : project_name,
    'project_path' : project_path,
    'project_repo' : project_repo
}
gitignore_content   = Template(requests.get("https://gitlab.com/commons-acp/templates/gitlab/docker-image/-/raw/master/.gitignore").text).substitute(d)
gitlab_ci_content   = Template(requests.get("https://gitlab.com/commons-acp/templates/gitlab/docker-image/-/raw/master/.gitlab-ci.yml").text).substitute(d)
pom_xml_content     = Template(requests.get("https://gitlab.com/commons-acp/templates/gitlab/docker-image/-/raw/master/pom.xml").text).substitute(d)
dockerfile_content  = Template(requests.get("https://gitlab.com/commons-acp/templates/gitlab/docker-image/-/raw/master/Dockerfile").text).substitute(d)

data= {
    "branch": "master",
    "commit_message": "initialize repo",
    "start_branch": "master",
    "actions": [
        {
            "action": "create",
            "file_path": ".gitignore",
            "content": gitignore_content
        },
        {
            "action": "create",
            "file_path": ".gitlab-ci.yml",
            "content": gitlab_ci_content
        },
        {
            "action": "create",
            "file_path": "pom.xml",
            "content": pom_xml_content
        },
        {
            "action": "create",
            "file_path": "Dockerfile",
            "content": dockerfile_content
        }
    ]
}

commit = project.commits.create(data)
