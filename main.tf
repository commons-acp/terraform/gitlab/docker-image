
terraform {

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 3.4"
    }
  }
}

variable "gitlab_group_id" {
    description = "group id of the project"
}
variable "name"{
    description = "project name"
}
variable "gitlab_token"{
    description = "gitlab user token"
}
variable "docker_repo"{
    description = "docker repo name"
}
// Create a project in the example group
resource "gitlab_project" "projet" {
  name         =  var.name
  description  = "An empty project"
  namespace_id = var.gitlab_group_id
  initialize_with_readme = true
  shared_runners_enabled = true
  visibility_level = "public"
}
resource "null_resource" "initialize_repo" {
  provisioner "local-exec" {
    command = "/usr/bin/python3.8 $PATH/initialize.py $TOKEN $PROJECT_ID $PROJECT_REPO"

    environment = {
      PATH = path.module
      TOKEN = var.gitlab_token
      PROJECT_ID = gitlab_project.projet.id
      PROJECT_REPO = var.docker_repo
    }
  }
}
